# PassionJam

**Game brief**


* Điều khiển bằng single-tap với mỗi lần tap sẽ móc dây về một điểm trên cây hai bên, lần lượt bên trái và bên phải. Tránh các vật cản và cầu gai nguy hiểm.
* Thu thập túi rác để nâng cấp kỹ năng  nam châm, lá chắn, phản lực và khả năng tích lũy móc.
* Thu thập lon và cốc nhựa để kích hoạt kỹ năng với mỗi 2 lần thu thập liên tiếp sẽ có khả năng thi triển ¾ kỹ năng ở trên: nam châm, lá chắn, phản lực. (khả năng tích lũy móc là khả năng bị động)
* Hãy cẩn thận, khi không móc dây bạn sẽ rơi và kết thúc màn chơi


**Yêu cầu để đạt trải nghiệm tốt nhất**


* Kích thước màn hình: 1080x1920px, hoặc 9:16
* Bật tối đa âm thanh và độ sáng màn hình
* Bộ nhớ tối thiểu 70MB
