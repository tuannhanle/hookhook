﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IBG
{

}
public class IsBG : MonoBehaviour, IBG
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var mTemp = collision.gameObject.GetComponent<GroupItem>();
        if(mTemp != null)
        {
            BackgroundInstantie.EvtBG();
        }
    }
}
