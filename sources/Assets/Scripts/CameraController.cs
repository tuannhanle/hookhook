﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CameraController : MonoBehaviour
{
    #region lazy singleton
    private static CameraController _instance;
    private CameraController() { }
    public static CameraController getInstance()
    {
        if (_instance == null)
        {
            _instance = new CameraController();
        }
        return _instance;
    }
    #endregion
    public Transform player, bg2, bg1 = null;


#pragma warning disable 0649
    private Vector3 startPos;
    private Vector3 cameraTargetPos, bg1TargetPos, bg2TargetPos = new Vector3();
    float _distance;
    int _screenLevel = 0;
    float _oldCamPos;
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        startPos = this.transform.position;
        _distance = Vector3.Distance(player.position, transform.position);
        _oldCamPos = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        //camera

        Vector3 targetPos = SetPos(cameraTargetPos, this.transform.position.x, player.position.y + _distance / 2, this.transform.position.z);
        if (this.transform.position.y - Vector3.Lerp(this.transform.position, targetPos, 0.2f).y < 0)
        {
            this.transform.position = Vector3.Lerp(this.transform.position, targetPos, 0.2f);

        }





    }
    public void ReplayCamera()
    {
        transform.position = startPos;

    }


    private Vector3 SetPos(Vector3 pos, float x, float y, float z)
    {
        pos.x = x;
        pos.y = y;
        pos.z = z;
        return pos;

    }
}
