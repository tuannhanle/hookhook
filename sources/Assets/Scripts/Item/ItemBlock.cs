﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBlock
{
    void Damage(bool isSaw);
}

public class ItemBlock : MonoBehaviour
{
#pragma warning disable 0649
    [SerializeField]
    private bool isSaw;

    private BoxCollider2D boxCollider2D;

    // Start is called before the first frame update
    void Start()
    {
        boxCollider2D = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var mTemp = collision.gameObject.GetComponent<IBlock>();
        var mShield = collision.gameObject.GetComponent<IEf_Shield>();
        if(mTemp != null)
        {
            mTemp.Damage(isSaw);
        }
        if(mShield != null && boxCollider2D != null){
            boxCollider2D.enabled = false;
            Debug.Log("False");
        }
    }
}
