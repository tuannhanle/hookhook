﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface ICoin
{
    void EarnCoin();
}

public class ItemCoin : MonoBehaviour
{
    [SerializeField]
    private CircleCollider2D _collider;

    public int speedFindPlayer;
    public delegate void OnEvent();
    public OnEvent OnDestroy;
#pragma warning disable 0649
    [SerializeField]
    private AudioClip _audioEarned;
    bool _IsMatchedMgCircle = false;
    // Start is called before the first frame update
    void Awake()
    {
        
        if (_collider == null)
        {
            _collider = GetComponent<CircleCollider2D>();
        }
        OnDestroy += () =>
        {
            Destroy(this.gameObject);
        };

    }
    private void FixedUpdate()
    {
        if (_IsMatchedMgCircle)
        {
            MoveToPlayer(Player.getInstance().gameObject, Time.deltaTime);
        }
    }
    private void MoveToPlayer(GameObject mPlayer, float mDeltaTime)
    {
        transform.position = Vector3.MoveTowards(transform.position, mPlayer.transform.position, mDeltaTime * speedFindPlayer);

    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        var coin = other.gameObject.GetComponent<ICoin>();
        var SkillMG = other.gameObject.GetComponent<ISkillMG>();
        if (coin != null)
        {
            coin.EarnCoin();
            SoundManager.getInstance().PlaySFX(_audioEarned);
            OnDestroy();

        }
        if (SkillMG != null)
        {
            _IsMatchedMgCircle = true;


            //chay toi hero

        }
    }
}
