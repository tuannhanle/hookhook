﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface ISkillMG
{
    void OnRadiusMG(float _timeLife);
    void WaitForOffRadiusMG(float _timeLife);

}
public class SkillMG : MonoBehaviour, ISkillMG
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    // Update is called once per frame
    void Update()
    {
        this.transform.Rotate(new Vector3(0, 0, -500) * Time.deltaTime);
    }
    public void WaitForOffRadiusMG(float _timeLife)
    {
        StartCoroutine(CORO_OffRadius(_timeLife));
    }
    IEnumerator CORO_OffRadius(float _timeLife)
    {
        yield return new WaitForSeconds(_timeLife);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
    }
    public void OnRadiusMG(float _timeLife)
    {
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<CircleCollider2D>().enabled = true;
        WaitForOffRadiusMG(_timeLife);
    }

}
