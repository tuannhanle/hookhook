﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface ISquare
{
    //void OnBoosts(float speed, int time);
}

public class ItemSquare : MonoBehaviour
{
    [SerializeField]
    private CircleCollider2D _collider = null;
    //private float _speed;
    //private int time;
#pragma warning disable 0649
    [SerializeField]
    private AudioClip _audioEarned;
    




    public delegate void OnEvent();
    public OnEvent OnDestroy = null;

    // Start is called before the first frame update
    void Awake()
    {
        //_speed = Define.ItemBoosts_speed;
        //time = Define.ItemBoosts_time;
        if (_collider==null)
        {
            _collider = GetComponent<CircleCollider2D>();
        }
        OnDestroy += () =>
        {
            Destroy(this.gameObject);
        };

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var boosts = other.gameObject.GetComponent<ISquare>();
        if (boosts !=null)
        {
            if (PlayerBag.getInstance().IsFirstItemInBag())
            {
                PlayerBag.getInstance().PutItem(true);
            }
            else
            {
                PlayerBag.getInstance().PutItem(true);
                PlayerBag.getInstance().OnMatchItemSlot();
            }
            Debug.Log("IteSquare");
            SoundManager.getInstance().GenNewSpeaker(_audioEarned);
            OnDestroy();

        }
    }
}
