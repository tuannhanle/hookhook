﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPlunger
{
    void AddPlunger();
    void SubPlunger();
}

public class ItemPlunger : MonoBehaviour
{
    [SerializeField]
    private AudioClip _audioEarned;
    bool _IsMatchedMgCircle = false;
    public int speedFindPlayer;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (_IsMatchedMgCircle)
        {
            MoveToPlayer(Player.getInstance().gameObject, Time.deltaTime);
        }
    }

    private void MoveToPlayer(GameObject mPlayer, float mDeltaTime)
    {
        transform.position = Vector3.MoveTowards(transform.position, mPlayer.transform.position, mDeltaTime * speedFindPlayer);

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        var mTemp = collision.gameObject.GetComponent<IPlunger>();
        var SkillMG = collision.gameObject.GetComponent<ISkillMG>();
        if (mTemp != null){
            mTemp.AddPlunger();
            SoundManager.getInstance().PlaySFX(_audioEarned);
            Destroy(this.gameObject);
        }
        if (SkillMG != null)
        {
            _IsMatchedMgCircle = true;
        }

    }
}
