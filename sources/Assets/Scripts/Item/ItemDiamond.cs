﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
internal interface IDiamond
{
    //void OnShield(int time);
}
public class ItemDiamond : MonoBehaviour
{
    [SerializeField]
    private CircleCollider2D _collider = null;

#pragma warning disable 0649
    [SerializeField]
    private AudioClip _audioEarned;


    public delegate void OnEvent();
    public OnEvent OnDestroy = null;

    // Start is called before the first frame update
    void Awake()
    {


        if (_collider==null)
        {
            _collider = GetComponent<CircleCollider2D>();
        }
        OnDestroy += () =>
        {
            Destroy(this.gameObject);
        };

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var boosts = other.gameObject.GetComponent<IDiamond>();
        if (boosts !=null)
        {
            if (PlayerBag.getInstance().IsFirstItemInBag())
            {
                 PlayerBag.getInstance().PutItem(false);
            }
            else
            {
                PlayerBag.getInstance().PutItem(false);
                PlayerBag.getInstance().OnMatchItemSlot();
            }

            //boosts.OnShield(time);

            Debug.Log("ItemDiamond");
            SoundManager.getInstance().GenNewSpeaker(_audioEarned);
            OnDestroy();
        }
        
    }
}
