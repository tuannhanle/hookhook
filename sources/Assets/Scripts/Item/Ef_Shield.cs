﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IEf_Shield
{
    void OnTimeShield (int _timeLife);
    void WaitForOffTimeShield(int _timeLife);

}
public class Ef_Shield : MonoBehaviour, IEf_Shield
{

    public void WaitForOffTimeShield(int _timeLife)
    {
        StartCoroutine(CORO_OffTime(_timeLife));
    }

    IEnumerator CORO_OffTime(int _timeLife)
    {
        yield return new WaitForSeconds(_timeLife);
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<CircleCollider2D>().enabled = false;
    }

    public void OnTimeShield(int _timeLife)
    {
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<CircleCollider2D>().enabled = true;
        WaitForOffTimeShield(_timeLife);
    }
}
