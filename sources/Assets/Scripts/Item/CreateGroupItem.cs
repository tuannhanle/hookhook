﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface GroupItem
{

}
/// <summary>
/// Sinh item theo hàng dọc
/// </summary>
public class CreateGroupItem : MonoBehaviour, GroupItem
{
    [System.Serializable]
    public class GroupItem
    {

        public GameObject groupItem;

        public Define.ItemType itemType;
    }
    [SerializeField]
    private float distanceGroup = 1.5F;
    public Transform parentItem;
    public GroupItem[] groupItemCreate;

    public GroupItem groupEmpty;

    private static int amount;

    private int randomNumber;
#pragma warning disable 0649
    [SerializeField]
    private bool isDeving;
#pragma warning disable 0649
    [SerializeField]
    private int _tangchiahet;

    void Start()
    {

        amount = 0;
        while (amount < 10)
        {
            int n = Random.Range(-1, groupItemCreate.Length);
            if (n >= 0)
            {
                GroupItem mTempGI = groupItemCreate[n];
                if(mTempGI.itemType == Define.ItemType.Block || mTempGI.itemType == Define.ItemType.BlockSaw)
                {

                    GameObject layer = GenOB(groupEmpty.groupItem);// Instantiate(groupEmpty.groupItem) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
                else
                {
                    GameObject layer = GenOB(mTempGI.groupItem);//Instantiate(mTempGI.groupItem) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
            }
            else
            {
                GameObject layer = GenOB(groupEmpty.groupItem);//Instantiate(groupEmpty.groupItem) as GameObject;
                layer.transform.position = new Vector2(0, amount * distanceGroup);
            }
            //Increase amount.
            amount++;
        }
    }
    GameObject GenOB(GameObject mSample)
    {
        return Instantiate(mSample, parentItem) as GameObject;
    }
    void Update()
    {

    }

    void OnTriggerEnter2D(Collider2D col)
    {
        GameObject mGameObject = null;
        var mItem = col.gameObject.GetComponent<IItem>();
        if (mItem != null) //Tao ra khi maincamera bat dau di chuyen len cham vao layer
        {
            if (amount % 20 == 0/*||  !isDeving*/)
            {
                foreach (GroupItem mObj in groupItemCreate)
                {
                    int tmp = Random.Range(0, 2);
                    switch (tmp)
                    {
                        case 0:
                            if (mObj.itemType == Define.ItemType.Block)
                            {
                                mGameObject = mObj.groupItem;

                            }
                            break;
                        default:
                            if (mObj.itemType == Define.ItemType.BlockSaw)
                            {
                                mGameObject = mObj.groupItem;
                            }
                            break;
                    }
                }
                if (mGameObject != null)
                {
                    GameObject layer = GenOB(mGameObject);//Instantiate(mGameObject) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
                else
                {
                    GameObject layer = GenOB(groupEmpty.groupItem);//Instantiate(groupEmpty.groupItem) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
            }
            //If 15 layers has been spawned.
            else if (amount % 15 == 0/*|| isDeving*/)
            {
                foreach (GroupItem mObj in groupItemCreate)
                {
                    int tmp = Random.Range(0, 3);
                    switch (tmp)
                    {
                        case 0:
                            if (mObj.itemType == Define.ItemType.GemDiamond)
                            {
                                mGameObject = mObj.groupItem;

                            }
                            break;
                        case 1:
                            if (mObj.itemType == Define.ItemType.GemSquares)
                            {
                                mGameObject = mObj.groupItem;
                            }
                            break;
                        default:
                            if (mObj.itemType == Define.ItemType.Plunger)
                            {
                                mGameObject = mObj.groupItem;
                            }
                            break;
                    }
                }
                if (mGameObject != null)
                {
                    GameObject layer = GenOB(mGameObject);//Instantiate(mGameObject) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
                else
                {
                    GameObject layer = GenOB(groupEmpty.groupItem);//Instantiate(groupEmpty.groupItem) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
            }
            //If 10 layers has been spawned.
            else if (amount % 10 == 0)
            {
                foreach (GroupItem mObj in groupItemCreate)
                {
                    int tmp = Random.Range(0, 3);
                    switch (tmp)
                    {
                        case 0:
                            if (mObj.itemType == Define.ItemType.GemDiamond)
                            {
                                mGameObject = mObj.groupItem;
                            }
                            break;
                        case 1:
                            if (mObj.itemType == Define.ItemType.GemSquares)
                            {
                                mGameObject = mObj.groupItem;
                            }
                            break;
                        default:
                            if (mObj.itemType == Define.ItemType.Plunger)
                            {
                                mGameObject = mObj.groupItem;
                            }
                            break;
                    }
                }
                if (mGameObject != null)
                {
                    GameObject layer = GenOB(mGameObject);//Instantiate(mGameObject) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
                else
                {
                    GameObject layer = GenOB(groupEmpty.groupItem);//Instantiate(groupEmpty.groupItem) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }

            }
            else if (amount % 5 == 0)
            {
                foreach (GroupItem mObj in groupItemCreate)
                {
                    if (mObj.itemType == Define.ItemType.Plunger)
                    {
                        mGameObject = mObj.groupItem;
                    }
                }
                if (mGameObject != null)
                {
                    //Spawn layer with random integer.
                    GameObject layer = GenOB(mGameObject) as GameObject;
                    //Place layer in specific position.
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
                else
                {
                    GameObject layer = GenOB(groupEmpty.groupItem);//Instantiate(groupEmpty.groupItem) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
            }
            else
            {
                randomNumber = Random.Range(0, 2);
                if (randomNumber > 0)
                {
                    foreach (GroupItem mObj in groupItemCreate)
                    {
                        int tmp = Random.Range(0, 2);
                        switch (tmp)
                        {
                            case 0:
                                if (mObj.itemType == Define.ItemType.Trash)
                                {
                                    mGameObject = mObj.groupItem;
                                }
                                break;
                            default:
                                if (mObj.itemType == Define.ItemType.Plunger)
                                {
                                    mGameObject = mObj.groupItem;
                                }
                                break;
                        }
                    }
                    if (mGameObject != null)
                    {
                        GameObject layer = GenOB(mGameObject);//Instantiate(mGameObject) as GameObject;
                        layer.transform.position = new Vector2(0, amount * distanceGroup);
                    }
                    else
                    {
                        GameObject layer = GenOB(groupEmpty.groupItem);//Instantiate(groupEmpty.groupItem) as GameObject;
                        layer.transform.position = new Vector2(0, amount * distanceGroup);
                    }
                }
                else
                {
                    GameObject layer = GenOB(groupEmpty.groupItem);//Instantiate(groupEmpty.groupItem) as GameObject;
                    layer.transform.position = new Vector2(0, amount * distanceGroup);
                }
            }
            //Increase amount.
            amount++;
        }
    }
}
