﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
internal interface IMg
{
    //void OnMg(int time);
}
public class ItemMG : MonoBehaviour
{
    [SerializeField]
    private CircleCollider2D _collider = null;
#pragma warning disable 0649
    [SerializeField]
    private AudioClip _audioEarned;


    public delegate void OnEvent();
    public OnEvent OnDestroy = null;

    // Start is called before the first frame update
    void Awake()
    {


        if (_collider==null)
        {
            _collider = GetComponent<CircleCollider2D>();
        }
        OnDestroy += () =>
        {
            Destroy(this.gameObject);
        };

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        var boosts = other.gameObject.GetComponent<IMg>();
        if (boosts !=null)
        {
            OnDestroy();
        }
        
    }
}
