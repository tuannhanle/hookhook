﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IItem
{

}
/// <summary>
/// Sinh item theo hàng ngang
/// </summary>
public class CreateItem : MonoBehaviour, IItem
{
#pragma warning disable 0649
    [SerializeField]
    private GameObject item;

    [SerializeField]
    public Define.ItemType itemType;
#pragma warning disable 0649

    private List<Transform> positionItem = new List<Transform>();

    [SerializeField]
    private int maxInstantiate;

    [SerializeField]
    private int maxNumberInstantiate;
    [SerializeField]
    private float minDistanceInstantite, maxDistanceInstantite;

    private List<Transform> positionItemTemp = new List<Transform>();



    //void Awake()
    //{
    //    if (_instance != null && _instance != this)
    //    {
    //        Destroy(this.gameObject);
    //    }
    //    else
    //    {
    //        _instance = this;
    //    }
    //}


    // Start is called before the first frame update
    void Start()
    {
        float mDistance = maxDistanceInstantite - minDistanceInstantite;
        float mChia = mDistance / maxNumberInstantiate;
        Vector3 mPos = new Vector3(minDistanceInstantite, 0, 0);
        for (int a = 0; a < maxNumberInstantiate + 1; a++)
        {
            GameObject mObj = new GameObject();
            mObj.name = "Game" + a;
            mObj.transform.SetParent(this.transform, false);
            mObj.transform.localPosition = mPos;
            positionItem.Add(mObj.transform);
            mPos.x += mChia;
        }

        if (itemType == Define.ItemType.Block || itemType == Define.ItemType.BlockSaw)
        {
            InstantiateRandomItemBlock(itemType);
        }
        else
        {
            InstantiateRandomItem(itemType);
        }

 
    }

    //void InstantiateItem()
    //{
    //    int mNumber = positionItem.Count;
    //    int mAmount = 0;
    //    Transform[] mTranform = positionItem.ToArray();
    //    while (mNumber > mAmount)
    //    {

    //        GameObject mItem = Instantiate(item, mTranform[mAmount]);
    //        mItem.SetActive(false);
    //        objectItem.Add(mItem);
    //        mAmount++;
    //    }
    //}

    void InstantiateRandomItem(Define.ItemType _itemType)
    {
        int mNumber = maxInstantiate;
        int mAmount = 0;
        int mCounter = 0;
        while (mNumber > mAmount)
        {
            Transform mTempTranfrom = randomTranformItem();
            if (positionItemTemp != null)
            {
                foreach (Transform mTran in positionItemTemp)
                {
                    if (mTempTranfrom.name == mTran.name)
                    {
                        mCounter++;
                    }
                }
            }
            if (item != null && mCounter == 0)
            {
                //Debug.Log(mTempTranfrom);
                GameObject mItem = Instantiate(item, mTempTranfrom);
                positionItemTemp.Add(mTempTranfrom);
                mAmount++;
            }
            mCounter = 0;

        }
    }

    void InstantiateRandomItemBlock(Define.ItemType _itemType)
    {
        int mNumber = maxInstantiate;
        int mAmount = 0;
        int mNumberRandom = Random.Range(0, 2);
        int right = positionItem.Count - 1;
        int left = 0;
        while (mNumber > mAmount)
        {
            if(mNumberRandom > 0)
            {
                GameObject mItem = Instantiate(item, randomTranformItemBlock()[left]);
                left++;
            }
            else
            {
                GameObject mItem = Instantiate(item, randomTranformItemBlock()[right]);
                right--;
            }
            mAmount++;
        }
    }

    //void EnableItem(ItemType _itemType)
    //{
    //    GameObject[] mGameObjectItem = objectItem.ToArray();
    //    for(int a = 0; a < mGameObjectItem.Length; a++)
    //    {
    //        mGameObjectItem[a].SetActive(false);
    //    }

    //    int mNumber = maxItemEnable(_itemType);
    //    for (int a = 0; a < mNumber; a++)
    //    {
    //        GameObject mItem = randomEnableItem();
    //        mItem.SetActive(true);
    //    }
    //}

    //int maxItemEnable(ItemType _itemType)
    //{
    //    int maxNumber = 0;
    //    if(_itemType == ItemType.Coin)
    //    {
    //        maxNumber = 8;
    //    }
    //    else if (_itemType == ItemType.Boot || _itemType == ItemType.Shield || _itemType == ItemType.MG)
    //    {
    //        maxNumber = 1;
    //    }
    //    else if(_itemType == ItemType.Block)
    //    {
    //        maxNumber = 3;
    //    }
    //    else
    //    {
    //        maxNumber = 0;
    //    }
    //    return maxNumber;
    //}


    ///// <summary>
    ///// So luong 1 loai item (theo loai) co the sinh ra toi da trong 1 hang
    ///// </summary>
    ///// <param name="_itemType"></param>
    ///// <returns></returns>
    //int maxItemInstantiate(Define.ItemType _itemType)
    //{
    //    int maxNumber = 0;
    //    if (_itemType == Define.ItemType.Trash)
    //    {
    //        maxNumber = 8;
    //    }
    //    else if (_itemType == Define.ItemType.GemDiamond || _itemType == Define.ItemType.GemSquares)
    //    {
    //        maxNumber = 1;
    //    }
    //    else if (_itemType == Define.ItemType.Block || _itemType == Define.ItemType.BlockSaw)
    //    {
    //        maxNumber = 2;
    //    }
    //    else
    //    {
    //        maxNumber = 0;
    //    }
    //    return maxNumber;
    //}

    //GameObject randomEnableItem()
    //{
    //    GameObject[] mGameObject = objectItem.ToArray();
    //    int mRandomNumber = Random.Range(0, objectItem.Count);
    //    return mGameObject[mRandomNumber];
    //}

    Transform randomTranformItem()
    {
        Transform[] mTranform = positionItem.ToArray();
        int mRandomNumber = Random.Range(0, positionItem.Count);
        return mTranform[mRandomNumber];
    }

    Transform[] randomTranformItemBlock()
    {
        Transform[] mTranform = positionItem.ToArray();

            return mTranform;      
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.tag == "Bottom")
        {
            Destroy(this.gameObject);
        }
    }
}
