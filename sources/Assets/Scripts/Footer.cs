﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public interface IFooter
{
    void OnFallDown();
}


public class Footer : MonoBehaviour
{
    [SerializeField]
    private BoxCollider2D _collider = null;
    [SerializeField]
    private AudioClip audiodie;


    // Start is called before the first frame update
    void Awake()
    {
        if (_collider == null)
        {
            _collider = GetComponent<BoxCollider2D>();
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var fallen = collision.gameObject.GetComponent<IFooter>();
        if (fallen != null)
        {
            fallen.OnFallDown();
            SoundManager.getInstance().PlaySFX(audiodie);
        }
    }

}
