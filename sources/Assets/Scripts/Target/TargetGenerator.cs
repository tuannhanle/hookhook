﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TargetGenerator : MonoBehaviour
{
    #region lazy singleton
    private static TargetGenerator _instance;
    private TargetGenerator() { }
    public static TargetGenerator getInstance()
    {
        if (_instance == null)
        {
            _instance = new TargetGenerator();
        }
        return _instance;
    }
    #endregion
#pragma warning disable 0649
    [SerializeField]
    Vector3 leftSide, rightSide;
#pragma warning disable 0649
    [SerializeField]
    GameObject sampleLeft;
    [SerializeField]
    GameObject sampleRight;
    [SerializeField]
    Transform parentTransform;
    public List<GameObject> LeftSideTargetList, RightSideTargetList;
    
    private  int amountLeft;
    private  int amountRight;
    public delegate void EventInstantiateTarget(bool _isLeft);
    public static EventInstantiateTarget EvtInstantiateTarget;


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        amountLeft = 0;
        amountRight = 0;
    }
    private void Start()
    {
        EvtInstantiateTarget += Handle_EvtInstantiateTarget;
        IdleProcess();
    }
    public void IdleProcess()
    {
        amountLeft = 0;
        amountRight = 0;
        LeftSideTargetList.Add(GenTarget(sampleLeft, leftSide, amountLeft * 6));
        RightSideTargetList.Add(GenTarget(sampleRight, rightSide, amountRight * 6 + 3));
        amountLeft++;
        amountRight++;
    }
    private void Handle_EvtInstantiateTarget(bool _isLeft)
    {
        if (_isLeft)
        {
            LeftSideTargetList.Add(GenTarget(sampleLeft, leftSide, (amountLeft) * 6));
            amountLeft++;
        }
        else
        {
            RightSideTargetList.Add(GenTarget(sampleRight, rightSide, (amountRight) * 6 + 3));
            amountRight++;
        }
    }

    GameObject GenTarget(GameObject mSample, Vector3 mParent, int distance)
    {
        GameObject go = Instantiate(mSample, parentTransform);
        go.transform.position = new Vector3(go.transform.position.x + mParent.x, go.transform.position.y + mParent.y + distance, go.transform.position.z);
        return go;

    }
   






}
