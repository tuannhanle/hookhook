﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Target : MonoBehaviour
{
    public bool IsVisible { get; set; }
    private void Awake()
    {
        IsVisible = false;
    }
    private void OnBecameVisible()
    {
        IsVisible = true;
    }
    private void OnBecameInvisible()
    {
        IsVisible = false;
    }
    [SerializeField]
    bool isLeft;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var mTemp = collision.gameObject.GetComponent<GroupItem>();
        var mDestroy = collision.gameObject.GetComponent<IDestroyTarget>();
        if (mTemp != null)
        {
            TargetGenerator.EvtInstantiateTarget(isLeft);
        }
        if(mDestroy != null)
        {
            Destroy(this.gameObject);
        }
    }
}
