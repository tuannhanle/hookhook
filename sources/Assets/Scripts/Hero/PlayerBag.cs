﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerBag : MonoBehaviour, IPlunger
{
    #region lazy singleton
    private static PlayerBag _instance;
    private PlayerBag() { }
    public static PlayerBag getInstance()
    {
        if (_instance == null)
        {
            _instance = new PlayerBag();
        }
        return _instance;
    }
    #endregion
#pragma warning disable 0649
    [SerializeField]
    List<bool> ItemSlots; //true = Square, false = diamond
    [SerializeField]
    SpriteRenderer _slot1;
    [SerializeField]
    Sprite _square, _diamond;
    private int _curCoin = 0;
    public int GetCurCoin
    {
        get { return _curCoin; }

    }
    private int _plungerCount;

    public int PlungerCount
    {
        get { return _plungerCount; }
        //set { _plungerCount = value; }
    }

    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _plungerCount = 30;
    }

    public void PutOneCoin()
    {
        _curCoin++;
        Credict.getInstance().AddCoinIntoBag(1);
    }
    public void PutSpeacialCoin(int mValue)
    {
        _curCoin += mValue;
        Credict.getInstance().AddCoinIntoBag(mValue);

    }
    public bool IsFirstItemInBag()
    {
        if (ItemSlots.Count==0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    public void OnMatchItemSlot()
    {
        Debug.Log("OnMatchItemSlot()");
        if (ItemSlots.ToArray()[0]==ItemSlots.ToArray()[1])
        {
            if (ItemSlots.ToArray()[0]) // true => Square //Boots
            {
                PlayerCollision.getInstance().OnSquareSquareSkill();
                Debug.Log("OnBootsSkill");

            }
            else //Shield
            {
                PlayerCollision.getInstance().OnDiamondDiamondSkill();
                Debug.Log("OnShieldSkill");

            }
        }
        else //MG
        {
            PlayerCollision.getInstance().OnDiamondSquareSkill();
            Debug.Log("OnMGSkill");
        }
        ItemSlots.Clear();
    }
    public void PutItem(bool mIsItemSquare)
    {
        
        if (IsFirstItemInBag())
        {
            if (mIsItemSquare)
            {
                _slot1.sprite = _square;
            }
            else
            {
                _slot1.sprite = _diamond;

            }
        }
        else
        {
            _slot1.sprite = null;
        }
        ItemSlots.Add(mIsItemSquare);


    }
    public void AddPlunger()
    {
        _plungerCount++;
        UI_GamePlay.getInstance().SetPlungerCounting(_plungerCount);
    }

    public void SubPlunger()
    {
        _plungerCount--;
        UI_GamePlay.getInstance().SetPlungerCounting(_plungerCount);
    }

}
