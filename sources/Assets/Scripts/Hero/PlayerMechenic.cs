﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMechenic : MonoBehaviour
{
    #region lazy singleton
    private static PlayerMechenic _instance;
    private PlayerMechenic() { }
    public static PlayerMechenic getInstance()
    {
        if (_instance == null)
        {
            _instance = new PlayerMechenic();
        }
        return _instance;
    }
    #endregion
    private Rigidbody2D _rigidbody2D;
    private LineRenderer _lineRendere;
    private DistanceJoint2D joint;
    private CircleCollider2D circleCollider2D;
    public float distance = 10f;
    public LayerMask mask;
    private float _step=0.2f;

    bool _firstTap = false;
 #pragma warning disable 0649
    [SerializeField]
    List<AudioClip> _SFX_Hook;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

        _lineRendere = GetComponent<LineRenderer>();
        _lineRendere.SetWidth(0.1f, 0.1f);
        _lineRendere.enabled = false;

        joint = GetComponent<DistanceJoint2D>();
        joint.enabled = false;

        _rigidbody2D = GetComponent<Rigidbody2D>();

    }
    Transform _currentTempNearestTarget = null;
    private bool _isSpeedUp = false;
    private void FixedUpdate()
    {
        if (joint.distance > 3f)
        {
        
            _step += 0.001f;
            joint.distance -= _step;
        }
        else
        {
            _step = 0.2f;
            _lineRendere.enabled = false;
            joint.enabled = false;
        }
        if (_isSpeedUp)
        {
            joint.enabled = false;
        }

    }
    private void Start()
    {
        circleCollider2D = GetComponent<CircleCollider2D>();
    }
    private void Update()
    {


        if (_lineRendere.enabled)
        {
            _lineRendere.SetPosition(0, this.transform.position);
            _lineRendere.SetPosition(1, _currentTempNearestTarget.position);
        }

    }
    public void OnTap()
    {
        if (PlayerBag.getInstance().PlungerCount>0)
        {
            SoundManager.getInstance().PlaySFX(_SFX_Hook.ToArray()[Random.Range(0, _SFX_Hook.Count)]);
            if (_currentTempNearestTarget != null)
            {
                Destroy(_currentTempNearestTarget.gameObject);
            }
            _firstTap = !_firstTap;
            joint.enabled = true;
            if (_firstTap)
            {
                _currentTempNearestTarget = FindNearestLeftSide().transform;
            }
            else
            {
                _currentTempNearestTarget = FindNearestRightSide().transform;
            }
            joint.connectedBody = _currentTempNearestTarget.GetComponent<Rigidbody2D>();
            Vector2 mTempVector = _currentTempNearestTarget.position;
            joint.connectedAnchor = mTempVector - new Vector2(_currentTempNearestTarget.position.x, _currentTempNearestTarget.position.y);
            joint.distance = Vector2.Distance(this.transform.position, _currentTempNearestTarget.transform.position);
            _lineRendere.enabled = true;
            PlayerBag.getInstance().SubPlunger();
        }
        else
        {
            Debug.Log("HET DAN");
        }


    }
    GameObject FindNearestLeftSide()
    {
        GameObject mCloset = null;
        float mDistance = Mathf.Infinity;
        Vector3 mPosition = transform.position;
        foreach (var item in TargetGenerator.getInstance().LeftSideTargetList)
        {
            if (item != null&& item.GetComponent<Target>().IsVisible && item.transform.position.y > mPosition.y)
            {
                Vector3 mDiff = item.transform.position - mPosition;
                float mCurDistance = mDiff.sqrMagnitude;
                if (mCurDistance < mDistance)
                {
                    mCloset = item;
                    mDistance = mCurDistance;

                }
            }

        }
        return mCloset;
    }
    GameObject FindNearestRightSide()
    {
        GameObject mCloset = null;
        float mDistance = Mathf.Infinity;
        Vector3 mPosition = transform.position;
        foreach (var item in TargetGenerator.getInstance().RightSideTargetList)
        {
            if (item != null && item.GetComponent<Target>().IsVisible)
            {
                Vector3 mDiff = item.transform.position - mPosition;
                float mCurDistance = mDiff.sqrMagnitude;
                if (mCurDistance < mDistance)
                {
                    mCloset = item;
                    mDistance = mCurDistance;

                }
            }

        }
        return mCloset;
    }

    public void SpeedUp()
    {
        _rigidbody2D.AddForce(Vector3.up * 4000);
        _isSpeedUp = true;
        StartCoroutine(DisableColliderinTime(1));
    }

    IEnumerator DisableColliderinTime(float _delay) //Tat collider cua player trong mot khoan thoi gian
    {
        yield return 0;
        if (circleCollider2D != null)
        {
            circleCollider2D.enabled = false;

            yield return new WaitForSeconds(_delay);
            _rigidbody2D.AddForce(Vector3.down * 3000);
            yield return new WaitForSeconds(0.5F);
            circleCollider2D.enabled = true;
        }
        
        _isSpeedUp = false;
    }
}
