﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCollision : MonoBehaviour, ISquare, ICoin, IDiamond, IMg , IFooter, IBlock
{
    #region lazy singleton
    private static PlayerCollision _instance;
    private PlayerCollision() { }
    public static PlayerCollision getInstance()
    {
        if (_instance == null)
        {
            _instance = new PlayerCollision();
        }
        return _instance;
    }
    #endregion
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }

    [SerializeField]
    private AudioClip sfx_MG, sfx_Shiled, sfx_Boots;
    public void OnSquareSquareSkill() //trc do la boosts
    {
        // tang toc' * _speed
        PlayerMechenic.getInstance().SpeedUp();
        SoundManager.getInstance().GenNewSpeaker(sfx_Boots);
    }
    public void EarnCoin()
    {
        //Player Score +1

        PlayerBag.getInstance().PutOneCoin();
        UI_GamePlay.getInstance().SetScore(PlayerBag.getInstance().GetCurCoin);
    }
    public void OnDiamondDiamondSkill() //trc do la Shield
    {
        //PlayerMechenic.getInstance().ShieldOn();
        Player.getInstance().effectShield.OnTimeShield(Define.ItemShield_time);
        SoundManager.getInstance().GenNewSpeaker(sfx_Shiled);
    }
    public void OnDiamondSquareSkill()  //trc do la MG
    {
        Player.getInstance().skillMG.OnRadiusMG(Define.ItemMG_time);
        SoundManager.getInstance().GenNewSpeaker(sfx_MG);
    }
    public void OnFallDown()
    {
        StageManager.getInstance().stage = StageManager.Stage.Die;
    }

    public void Damage(bool isSaw)
    {
        if (isSaw)
        {
            StageManager.getInstance().stage = StageManager.Stage.Die;
        }
    }


}
