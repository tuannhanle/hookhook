﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    #region lazy singleton
    private static Player _instance;
    private Player() { }
    public static Player getInstance()
    {
        if (_instance == null)
        {
            _instance = new Player();
        }
        return _instance;
    }
    #endregion
 
    public GameObject platform;
    Vector3 startPosition;
    private int _lastDistance = 0;
    private int _highDistance = 0;
    public int HighDistance { get { return _highDistance; } set { _highDistance = value; } }
    public int LastDistance
    {
        get { return _lastDistance; }
        set { _lastDistance = value; }
    }



    private int _levelManetize = 1;
    public int LevelManetize { get { return _levelManetize; } set { _levelManetize = value; } }
    private int _levelPushing =1;
    public int LevelPushing { get { return _levelPushing; } set { _levelPushing = value; } }
    private int _levelShield = 1;
    public int LevelShield { get { return _levelShield; } set { _levelShield = value; } }
    private int _levelTimes = 1;
    public int LevelTimes { get { return _levelTimes; } set { _levelTimes = value; } }




    public SkillMG skillMG;
    public Ef_Shield effectShield;
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        //MG_Circle.SetActive(false);
       

        startPosition = this.transform.position;
        _levelManetize = PlayerPrefs.GetInt("levelManetize", LevelManetize);
        _levelPushing = PlayerPrefs.GetInt("levelPushing", LevelPushing);
        _levelShield = PlayerPrefs.GetInt("levelShield", LevelShield);
        _levelTimes = PlayerPrefs.GetInt("levelTimes", LevelTimes);
        _highDistance = PlayerPrefs.GetInt("highDistance", HighDistance);
        


    }

    void Update()
    {



    }
    public void UpLevel(int _type)
    {
        switch (_type)
        {
            case 1:
                if (_levelManetize<10)
                {
                    PlayerPrefs.SetInt("levelManetize", LevelManetize++);
                    //_levelManetize = PlayerPrefs.GetInt("levelManetize", LevelManetize);
                    PlayerBag.getInstance().PutSpeacialCoin(-150);
                }

                break;
            case 2:
                if (_levelPushing<10)
                {
                    PlayerPrefs.SetInt("levelPushing", LevelPushing++);
                    //_levelPushing = PlayerPrefs.GetInt("levelPushing", LevelPushing);
                    PlayerBag.getInstance().PutSpeacialCoin(-150);

                }

                break;
            case 3:
                if (_levelShield<10)
                {
                    PlayerPrefs.SetInt("levelShield", LevelShield++);
                    //_levelShield = PlayerPrefs.GetInt("levelShield", LevelShield);
                    PlayerBag.getInstance().PutSpeacialCoin(-150);

                }

                break;
            case 4:
                if (_levelTimes<10)
                {
                    PlayerPrefs.SetInt("levelTimes", LevelTimes++);
                    //_levelTimes = PlayerPrefs.GetInt("levelTimes", LevelTimes);
                    PlayerBag.getInstance().PutSpeacialCoin(-150);

                }

                break;

            default:
                break;
        }
    }
    public  void DistanceAndHighDistance()
    {
        LastDistance = Mathf.FloorToInt(Vector3.Distance(startPosition, this.transform.position));
        if (LastDistance > HighDistance)
        {
            HighDistance = LastDistance;
            PlayerPrefs.SetInt("highDistance", HighDistance);

        }
    }
    public void IdleProcess()
    {
        transform.position = startPosition;
    }
    public void OnIdle(bool mIsIdle)
    {
        platform.SetActive(mIsIdle);
    }
    public void GameController()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            PlayerMechenic.getInstance().OnTap();


        }
    }

}
