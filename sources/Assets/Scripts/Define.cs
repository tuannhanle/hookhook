﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Define : MonoBehaviour
{
    public static int VolumeBGM = 1; //0->1
    public static int VolumeSFX = 1; //0->1


    public static float ItemBoosts_speed = 2;
    public static int ItemCoin_value = 1;

    public static int ItemPlunger_counter = 10;

    public static int ItemDiamond_time = 3;
    public static int ItemMG_time = 3;
    public static int ItemBoosts_time = 3;
    public static int ItemShield_time = 5;

    public enum ItemType
    {
        Trash,
        GemDiamond,
        GemSquares,
        Block,
        BlockSaw,
        Plunger,
        Empty
    }

}
