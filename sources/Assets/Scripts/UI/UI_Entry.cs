﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UI_Entry : MonoBehaviour
{
    #region lazy singleton
    private static UI_Entry _instance;
    private UI_Entry() { }
    public static UI_Entry getInstance()
    {
        if (_instance == null)
        {
            _instance = new UI_Entry();
        }
        return _instance;
    }
    #endregion
#pragma warning disable 0649
    [SerializeField]
    Button startButton,upgradeButton;

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        startButton.onClick.AddListener(() =>
        {
            StageManager.getInstance().stage = StageManager.Stage.Playing;
            PlayerMechenic.getInstance().OnTap();

            this.gameObject.SetActive(false);
        });
        upgradeButton.onClick.AddListener(() =>
        {
            UI_Upgrade.getInstance().OnOpen();
        });
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
