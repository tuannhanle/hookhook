﻿
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageManager : MonoBehaviour
{
    #region lazy singleton
    private static StageManager _instance;
    private StageManager() { }
    public static StageManager getInstance()
    {
        if (_instance == null)
        {
            _instance = new StageManager();
        }
        return _instance;
    }
    #endregion
    // Start is called before the first frame update
    public enum Stage
    {
        Replay,Idle, Playing, Pause, Die
    }
    public Stage stage;

    [SerializeField]
    ItemParent _itemParent;
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        
    }
    private void Start()
    {
        stage = Stage.Idle;
    }
    // Update is called once per frame
    void Update()
    {
        switch (stage)
        {
            case Stage.Replay:
                _itemParent.ClearHisChilds();
                TargetGenerator.getInstance().IdleProcess();
                stage = Stage.Idle;

                break;
            case Stage.Idle:

                Player.getInstance().IdleProcess();
                CameraController.getInstance().ReplayCamera();
                UI_GamePlay.getInstance().SetDistance(0);
                Player.getInstance().OnIdle(true);

                break;
            case Stage.Playing:
                Player.getInstance().GameController();
                Player.getInstance().DistanceAndHighDistance();
                UI_GamePlay.getInstance().SetDistance(Player.getInstance().LastDistance);
                Player.getInstance().OnIdle(false);

                break;
            case Stage.Pause:
                break;
            case Stage.Die:
                GameUIManager.getInstance().UI_LoseWindow.OnOpen();
                SoundManager.getInstance().MuteBGM();
                break;
            default:
                break;
        }
    }


}
