﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{
    
    #region lazy singleton
    private static SoundManager _instance;
    private SoundManager() { }
    public static SoundManager getInstance()
    {
        if (_instance == null)
        {
            _instance = new SoundManager();
        }
        return _instance;
    }
    #endregion
    [SerializeField]
    private AudioSource _audioSourceBgm, _audioSourceSfx = null;
    [SerializeField]
    private List<AudioClip> _BgmList = null;
    [SerializeField]
    private GameObject _speakerAtom;
    [SerializeField]
    Transform _parentSpeakerGroup;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        if (_audioSourceBgm == null)
        {
            _audioSourceBgm = GetComponent<AudioSource>();

        }
 
    }

    // Start is called before the first frame update
    private void Update()
    {

    }
    private void Start()
    {
        
        PlayBGM();
        _audioSourceBgm.volume = 1;
    }
    public void PlaySFX(AudioClip audioclip)
    {
        _audioSourceSfx.volume = Define.VolumeSFX;
        _audioSourceSfx.clip = audioclip;
        _audioSourceSfx.Play();
    }
    public void PlayBGM()
    {
        foreach (AudioClip audio in _BgmList)
        {
            _audioSourceBgm.volume = Define.VolumeBGM;
            _audioSourceBgm.clip = audio;
            _audioSourceBgm.Play();
        }

    }
    public void MuteBGM()
    {
        Define.VolumeBGM = 0;
        _audioSourceBgm.volume = 0;

    }
    public void MuteSFX()
    {
        Define.VolumeSFX = 0;
        _audioSourceSfx.volume = 0;
    }
    public void GenNewSpeaker(AudioClip mAudioClip)
    {
        GameObject spAtom = Instantiate(_speakerAtom, _parentSpeakerGroup);
        spAtom.GetComponent<SpeakerAtom>().PlaySFX(mAudioClip);
    }
}
