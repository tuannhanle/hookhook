﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundController : MonoBehaviour
{
    #region lazy singleton
    private static BackgroundController _instance;
    private BackgroundController() { }
    public static BackgroundController getInstance()
    {
        if (_instance == null)
        {
            _instance = new BackgroundController();
        }
        return _instance;
    }
    #endregion
#pragma warning disable 0649
    [SerializeField]
    List<Sprite> ImagesList;
    public Transform player, bg2, bg1 = null;
    int _screenLevel = 0;
    private float size;
#pragma warning disable 0649
    float _oldCamPos;
    private Vector3  bg1TargetPos, bg2TargetPos = new Vector3();
    void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }
    // Start is called before the first frame update
    void Start()
    {
        size = bg2.GetComponent<BoxCollider2D>().size.y;
    }

    // Update is called once per frame
    void Update()
    {
        //bg
        if (transform.position.y >= bg1.position.y)
        {
            _screenLevel++;

            bg2.position = SetPos(bg1TargetPos, bg2.position.x, bg1.position.y + size, bg2.position.z);

            SwitchBg();


        }


        //if (transform.position.y < bg2.position.y)
        //{
        //    _screenLevel--;

        //    bg1.position = SetPos(bg2TargetPos, bg1.position.x, bg2.position.y - size, bg1.position.z);
        //    SwitchBg();
        //}
    }
    void SwitchBg()
    {

        if (_screenLevel == 2)
        {
            ChangeBackgroundSprite(ImagesList.ToArray()[1], bg2);
        }

        if (_screenLevel == 4)
        {
            ChangeBackgroundSprite(ImagesList.ToArray()[3], bg2);
            ChangeBackgroundSprite(ImagesList.ToArray()[2], bg1);

        }
        if (_screenLevel == 5)
        {
            ChangeBackgroundSprite(ImagesList.ToArray()[3], bg2);

        }

        if (_screenLevel == 6)
        {
            ChangeBackgroundSprite(ImagesList.ToArray()[5], bg2);
            ChangeBackgroundSprite(ImagesList.ToArray()[4], bg1);

        }
        if (_screenLevel == 7)
        {
            ChangeBackgroundSprite(ImagesList.ToArray()[5], bg2);

        }
        Transform temp = bg2;
        bg2 = bg1;
        bg1 = temp;
    }
    void ChangeBackgroundSprite(Sprite _sprite, Transform _object)
    {
        _object.GetComponent<SpriteRenderer>().sprite = _sprite;

    }
    private Vector3 SetPos(Vector3 pos, float x, float y, float z)
    {
        pos.x = x;
        pos.y = y;
        pos.z = z;
        return pos;

    }
}
