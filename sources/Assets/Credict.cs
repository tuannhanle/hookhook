﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Credict : MonoBehaviour
{
    #region lazy singleton
    private static Credict _instance;
    private Credict() { }
    public static Credict getInstance()
    {
        if (_instance == null)
        {
            _instance = new Credict();
        }
        return _instance;
    }
    #endregion
    private int _coinInBag=0;

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
        _coinInBag= PlayerPrefs.GetInt("coinInBag", _coinInBag); 
    }
    private void Update()
    {

    }
    public int GetCoinInBag
    {
        get { return _coinInBag; }
        //set { _coinInBag = value; }
    }
    public void AddCoinIntoBag(int mValue)
    {
        _coinInBag += mValue;
        PlayerPrefs.SetInt("coinInBag", _coinInBag);

    }

    public void SubCoinInBag(int mValue)
    {
        _coinInBag -= mValue;
        PlayerPrefs.SetInt("coinInBag", _coinInBag);


    }
}
