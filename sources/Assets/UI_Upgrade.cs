﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UI_Upgrade : MonoBehaviour
{
    #region lazy singleton
    private static UI_Upgrade _instance;
    private UI_Upgrade() { }
    public static UI_Upgrade getInstance()
    {
        if (_instance == null)
        {
            _instance = new UI_Upgrade();
        }
        return _instance;
    }
    #endregion


    [SerializeField]
    GameObject Panel;
    [SerializeField]
    Button ManeButton, PushButton, ShieldButton, TimesButton, OkButton;
    [SerializeField]
    Text ManeText, PushText, ShieldText, TimesText;
    // Start is called before the first frame update
    private void Awake()
    {

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }

    }
    void Start()
    {
        UpdateText();
        //P/s: Gap qua khong kip code cho nay, argument = int hoi bi sida, bo qua nha :D
        ManeButton.onClick.AddListener(() =>
        {
            Player.getInstance().UpLevel(1);
            UpdateText();
        });
        PushButton.onClick.AddListener(() =>
        {
            Player.getInstance().UpLevel(2);
            UpdateText();
        });
        ShieldButton.onClick.AddListener(() =>
        {
            Player.getInstance().UpLevel(3);
            UpdateText();
        });
        TimesButton.onClick.AddListener(() =>
        {
            Player.getInstance().UpLevel(4);
            UpdateText();
        });
        OkButton.onClick.AddListener(() =>
        {
            OnClosed();
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
    public void UpdateText()
    {
        ManeText.text = Player.getInstance().LevelManetize.ToString();
        PushText.text = Player.getInstance().LevelPushing.ToString();
        ShieldText.text = Player.getInstance().LevelShield.ToString();
        TimesText.text = Player.getInstance().LevelTimes.ToString();
    }
    public void OnOpen()
    {

        Panel.SetActive(true);

    }
    public void OnClosed()
    {

        Panel.SetActive(false);

    }
}
