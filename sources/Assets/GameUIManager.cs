﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class GameUIManager : MonoBehaviour
{
    #region lazy singleton
    private static GameUIManager _instance;
    private GameUIManager() { }
    public static GameUIManager getInstance()
    {
        if (_instance == null)
        {
            _instance = new GameUIManager();
        }
        return _instance;
    }
    #endregion
    private void Awake()
    {

        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
#pragma warning disable 0649
    [SerializeField]
    Button _inputSensor;
    [SerializeField]
    public UI_LoseWindow UI_LoseWindow;
    bool iSPause = false;
    // Start is called before the first frame update
    void Start()
    {
        _inputSensor.onClick.AddListener(() =>
        {
            if (iSPause)
            {

            }
            else
            {
                PlayerMechenic.getInstance().OnTap();
            }
            
        });
    }

    // Update is called once per frame
    void Update()
    {

    }
    
    public void OnPause()
    {
        iSPause = true;
    }
    public void OnPlay()
    {
        iSPause = false;
    }
}
