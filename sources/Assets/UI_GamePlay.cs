﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_GamePlay : MonoBehaviour
{
    #region lazy singleton
    private static UI_GamePlay _instance;
    private UI_GamePlay() { }
    public static UI_GamePlay getInstance()
    {
        if (_instance == null)
        {
            _instance = new UI_GamePlay();
        }
        return _instance;
    }
    #endregion


#pragma warning disable 0649
    [SerializeField]
    Text _ScoreText, _DistanceText,_PlungerCounting;
#pragma warning disable 0649
    [SerializeField]
    Button PauseButton;
    [SerializeField]
    Image PauseImage;
    [SerializeField]
    Sprite PlaySprite, PauseSprite;
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        else
        {
            _instance = this;
        }
    }
    bool _firstClicked = false;
    private void Start()
    {
        PauseButton.onClick.AddListener(() =>
        {
            _firstClicked = !_firstClicked;
            if (_firstClicked)//pause
            {
                Time.timeScale = 0;
                PauseImage.sprite = PlaySprite;
                GameUIManager.getInstance().OnPause();
            }
            else//play
            {
                Time.timeScale = 1;
                PauseImage.sprite = PauseSprite;
                GameUIManager.getInstance().OnPlay();
            }


        });
    }
    public void SetScore(int _value)
    {
        _ScoreText.text = _value.ToString();

    }
    public void SetDistance(int _value)
    {
        _DistanceText.text = _value.ToString();
    }
    public void SetPlungerCounting(int _value)
    {
        _PlungerCounting.text ="X "+ _value.ToString();
    }
}
