﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackgroundInstantie : MonoBehaviour
{
    [SerializeField]
    List<Sprite> imageList;
    [SerializeField]
    private GameObject prefabBackground;

    [SerializeField]
    private Transform tranformParent;

    private float sizeBackground = 23.13253F;

    private int amount;
    private int amountBG;

    public delegate void EventBG();
    public static EventBG EvtBG;
    // Start is called before the first frame update
    void Start()
    {
        EvtBG += Handle_InsBG;
        amount = amountBG = 0;
        BoxCollider2D[] mBox = prefabBackground.GetComponents<BoxCollider2D>();
        if(mBox != null)
        {
            foreach(BoxCollider2D m in mBox)
            {
                if(m.size.y > sizeBackground)
                {
                    sizeBackground = m.size.y;
                }
            }
        }

        IntantiateBackgroud();
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    void Handle_InsBG()
    {
        IntantiateBackgroud();

    }
    void IntantiateBackgroud()
    {
        GameObject mGame = Instantiate(prefabBackground, tranformParent);
        SpriteRenderer mSprRen = mGame.GetComponent<SpriteRenderer>();
        if (mSprRen != null)
        {
            mSprRen.sprite = imageList.ToArray()[amountBG];
            Debug.Log(amountBG);
            amountBG++;
            if (amountBG > (imageList.Count - 1))
            {
                amountBG = 0;
            }
        }
        mGame.transform.position = new Vector3(0, sizeBackground * amount, 0);
        amount++;
    }
}
