﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class UI_LoseWindow : MonoBehaviour
{
    [SerializeField]
    GameObject _loseWindow;
#pragma warning disable 0649
    [SerializeField]
    Button backButton, replayButton, upgradeButton;
#pragma warning disable 0649
    [SerializeField]
    Text highscoreText, scoreText, coinText;
    // Start is called before the first frame update
    void Start()
    {
        backButton.onClick.AddListener(() => {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");

        });
        replayButton.onClick.AddListener(() => {
            UnityEngine.SceneManagement.SceneManager.LoadScene("SampleScene");

            //GameManager.getInstance().sceneManager.LoadSceneName("Middlescene");
            //OnReplay();

        });
        upgradeButton.onClick.AddListener(() => {
            UI_Upgrade.getInstance().OnOpen();
        });
    }

    public void OnOpen()
    {
        OnShowText();
        _loseWindow.SetActive(true);
        
    }
    //private void OnReplay()
    //{
    //    StageManager.getInstance().stage = StageManager.Stage.Replay;
    //    _loseWindow.SetActive(false);

    //}
    private void OnShowText()
    {
        highscoreText.text = Player.getInstance().HighDistance.ToString(); 
        scoreText.text = Player.getInstance().LastDistance.ToString();
        coinText.text ="X " + PlayerBag.getInstance().GetCurCoin.ToString(); 


    }

}
