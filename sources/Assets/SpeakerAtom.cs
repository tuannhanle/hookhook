﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeakerAtom : MonoBehaviour
{
    [SerializeField]
    private AudioSource  _audioSourceSfx = null;
    // Start is called before the first frame update
    private void Update()
    {
        if (!_audioSourceSfx.isPlaying)
        {
            Destroy(gameObject);
        }
    }

    public void PlaySFX(AudioClip audioclip)
    {
        _audioSourceSfx.volume = Define.VolumeSFX;
        _audioSourceSfx.clip = audioclip;
        _audioSourceSfx.Play();

    }
}
