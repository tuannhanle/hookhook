﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemParent : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
 
    public void ClearHisChilds()
    {
        
        for (int i = 0; i < GetComponentsInChildren<Transform>().Length; i++)
        {
            if (i == 0)
            {

            }
            else
            {
                Destroy(GetComponentsInChildren<Transform>()[i].gameObject);

            }
        }

    }
}
